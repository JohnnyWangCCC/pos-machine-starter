### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?
Today the course introduced the Tasking theory in the morning, and the case of putting in the refrigerator through Elephant to vividly explain the analysis of the process; and introduce the concept of Context Map. At the same time, in the case The basic command of Git introduces the concept of small steps. In the afternoon, the course analyzed the case through Tasking with Context Map through multiple practical cases, and the teacher led everyone to write code by the teacher. What impressed me was that a problem became organized, clear and clear after the decomposition of Tasking and Context Map.
### R (Reflective): Please use one word to express your feelings about today's class.
Wonderful.
### (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?
The most meaningful part of today's course is to taught us how to decompose a problem into different sub -tasks. Finally, the coding is clear, clear and easy to understand, which greatly improves our awareness of the code structure.
### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?
I will use the knowledge learned today into actual project work, which is of great significance to us to develop.