package pos.machine;

import java.util.*;
import java.util.stream.Collectors;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = decodeToItems(barcodes);
        Receipt receipt = calculateCost(receiptItems);
        return renderReceipt(receipt);
    }

    public List<Item> loadAllItems(){
        List<Item> data = new ArrayList<>();
        data.add(new Item("ITEM000000", "Coca-Cola", 3));
        data.add(new Item("ITEM000001", "Sprite", 3));
        data.add(new Item("ITEM000004", "Battery", 2));
        return data;
    }

    public List<ReceiptItem> decodeToItems(List<String> barcodes){
        Map<String, Item> cacheItems = parseAllItems();
        List<ReceiptItem> receiptItems = new ArrayList<>();
        Map<String, Integer> receiptItemsMap = new HashMap<>();
        for (String barcode : barcodes) {
            if (receiptItemsMap.containsKey(barcode)){
                receiptItemsMap.put(barcode, receiptItemsMap.get(barcode)+1);
            }else {
                receiptItemsMap.put(barcode,1);
            }
        }
        for (Map.Entry<String, Integer> receiptItemEntry : receiptItemsMap.entrySet()) {
            Item item = cacheItems.get(receiptItemEntry.getKey());
            receiptItems.add(new ReceiptItem(item.getName(),receiptItemEntry.getValue(),item.getPrice()));
        }
        return receiptItems;
    }

    public Map<String,Item> parseAllItems(){
        List<Item> items = loadAllItems();
        Map<String, Item> cacheItem = items.stream().collect(Collectors.toMap(Item::getBarcode, item -> item));
        return cacheItem;
    }

    public List<ReceiptItem> calculateItemsCost(List<ReceiptItem> receiptItems){
        for (ReceiptItem receiptItem : receiptItems) {
            receiptItem.setSubTotal(receiptItem.getUnitPrice() * receiptItem.getQuantity());
        }
        return receiptItems;
    }

    public int calculateTotalPrice(List<ReceiptItem> receiptItems){
        int totalPrice = 0;
        for (ReceiptItem receiptItem : receiptItems) {
            totalPrice += receiptItem.getSubTotal();
        }
        return totalPrice;
    }

    public Receipt calculateCost(List<ReceiptItem> receiptItems){
        List<ReceiptItem> receiptItemsTmp = calculateItemsCost(receiptItems);
        return new Receipt(receiptItemsTmp, calculateTotalPrice(receiptItemsTmp));
    }

    public String generateItemsReceipt(Receipt receipt){
        StringJoiner stringJoiner = new StringJoiner("\n");
        for (ReceiptItem receiptItem : receipt.getReceiptItems()) {
            String formatStringItem = String.format("Name: %s, Quantity: %d, Unit price: %d (yuan), Subtotal: %d (yuan)", receiptItem.getName(), receiptItem.getQuantity(), receiptItem.getUnitPrice(), receiptItem.getSubTotal());
            stringJoiner.add(formatStringItem);
        }
        return stringJoiner.toString();
    }

    public String generateReceipt(String itemsReceipt, int total){
        StringJoiner stringJoiner = new StringJoiner("\n");
        stringJoiner.add("***<store earning no money>Receipt***");
        stringJoiner.add(itemsReceipt);
        stringJoiner.add("----------------------");
        stringJoiner.add(String.format("Total: %d (yuan)", total));
        stringJoiner.add("**********************");
        return stringJoiner.toString();
    }

    public String renderReceipt(Receipt receipt){
        String itemsReceipt = generateItemsReceipt(receipt);
        return generateReceipt(itemsReceipt, receipt.getTotalPrice());
    }

}
